// -------------------------stage1-------------------------
var ran1,
    ran2,
    plusMinus = '';
function check1() {
    var int1 = document.getElementById('input1').value
    var n1 = parseInt(int1)
    var sumPlus = ran1 + ran2;
    var sumMinus = 0;
    if (ran1 < ran2) {
        sumMinus = ran2 - ran1;
    } else {
        sumMinus = ran1 - ran2;
    }

    if (isNaN(n1)) {
        document.getElementById('show').setAttribute('color', 'red')
        show.innerHTML = 'กรุณากรอกตัวเลขให้ครบ'
    } else if (plusMinus == '+' && n1 == sumPlus) {
        document.getElementById('show').setAttribute('color', 'yellow')
        show.innerHTML = 'ถูก'
        animationMc();
        hit1();
    } else if (plusMinus == '-' && n1 == sumMinus) {
        document.getElementById('show').setAttribute('color', 'yellow')
        show.innerHTML = 'ถูก'
        animationMc();
        hit1();
    } else {
        document.getElementById('show').setAttribute('color', 'red')
        show.innerHTML = 'ผิด'
        hpMc1();
        animationGo();
    }
}
function hpMc1() {
    let health1 = document.getElementById("health1")
    let btn = document.getElementById("btn")
    let btn2 = document.getElementById("btnplayagain")
    health1.value -= 35;
    if (health1.value <= 0) {
        mcDie();
        btn.style.visibility = "hidden";
        btn2.style.visibility = "visible";
    }
}
function animationMc() {
    setTimeout(animationMc2, 100)
    setTimeout(animationMc3, 180)
    setTimeout(animationMc4, 260)
    setTimeout(animationMc5, 340)
    setTimeout(animationMc6, 420)
    setTimeout(animationMc7, 500)
    setTimeout(animationMc1, 580)
}
function animationGo() {
    setTimeout(animationGo1, 100)
    setTimeout(animationGo2, 220)
    setTimeout(animationGo3, 260)
    setTimeout(animationGo4, 340)
    setTimeout(animationGo5, 420)
    setTimeout(animationGo6, 500)
    setTimeout(animationGo7, 580)
    setTimeout(animationGoStand, 660)
    

}
function animationGoDie() {
    setTimeout(animationGoDie1, 100)
    setTimeout(animationGoDie2, 180)
    setTimeout(animationGoDie3, 260)
    setTimeout(animationGoDie4, 340)
    setTimeout(animationGoDie5, 420)
    setTimeout(animationGoDie6, 500)
    setTimeout(animationGoDie7, 580)
}
function mcDie() {
    setTimeout(animationMcDie1, 100)
    setTimeout(animationMcDie2, 180)
    setTimeout(animationMcDie3, 260)
    setTimeout(animationMcDie4, 340)
    setTimeout(animationMcDie5, 420)
    setTimeout(animationMcDie6, 500)
    setTimeout(animationMcDie7, 580)
    setTimeout(animationMcDie8, 660)
}
// -------------------------mc atk---------------------
function animationMc1() {
    document.getElementById("mc").src = "./img/mc/character1.png";
}
function animationMc2() {
    document.getElementById("mc").src = "./img/mc/character2.png";
}
function animationMc3() {
    document.getElementById("mc").src = "./img/mc/character3.png";
}
function animationMc4() {
    document.getElementById("mc").src = "./img/mc/character4.png";
}
function animationMc5() {
    document.getElementById("mc").src = "./img/mc/character5.png";
}
function animationMc6() {
    document.getElementById("mc").src = "./img/mc/character6.png";
}
function animationMc7() {
    document.getElementById("mc").src = "./img/mc/character7.png";
}
// -------------------------mc die -----------------
function animationMcDie1() {
    document.getElementById("mc").src = "./img/mc/die/mcDying1.png";
}
function animationMcDie2() {
    document.getElementById("mc").src = "./img/mc/die/mcDying2.png";
}
function animationMcDie3() {
    document.getElementById("mc").src = "./img/mc/die/mcDying3.png";
}
function animationMcDie4() {
    document.getElementById("mc").src = "./img/mc/die/mcDying4.png";
}
function animationMcDie5() {
    document.getElementById("mc").src = "./img/mc/die/mcDying5.png";
}
function animationMcDie6() {
    document.getElementById("mc").src = "./img/mc/die/mcDying6.png";
}
function animationMcDie7() {
    document.getElementById("mc").src = "./img/mc/die/mcDying7.png";
}
function animationMcDie8() {
    document.getElementById("mc").src = "./img/mc/die/mcDying8.png";
}
// --------------------Golem die------------------
function animationGoDie1() {
    document.getElementById("monster").src = "./img/golemcha/die/golemdie1.png";
}
function animationGoDie2() {
    document.getElementById("monster").src = "./img/golemcha/die/golemdie2.png";
}
function animationGoDie3() {
    document.getElementById("monster").src = "./img/golemcha/die/golemdie3.png";
}
function animationGoDie4() {
    document.getElementById("monster").src = "./img/golemcha/die/golemdie4.png";
}
function animationGoDie5() {
    document.getElementById("monster").src = "./img/golemcha/die/golemdie5.png";
}
function animationGoDie6() {
    document.getElementById("monster").src = "./img/golemcha/die/golemdie6.png";
}
function animationGoDie7() {
    document.getElementById("monster").src = "./img/golemcha/die/golemdie7.png";
}
// --------------------Golem atk-----------------------
function animationGo1() {
    document.getElementById("monster").src = "./img/golemcha/golem1.png";
}
function animationGo2() {
    document.getElementById("monster").src = "./img/golemcha/golem2.png";
}
function animationGo3() {
    document.getElementById("monster").src = "./img/golemcha/golem3.png";
}
function animationGo4() {
    document.getElementById("monster").src = "./img/golemcha/golem4.png";
}
function animationGo5() {
    document.getElementById("monster").src = "./img/golemcha/golem5.png";
}
function animationGo6() {
    document.getElementById("monster").src = "./img/golemcha/golem6.png";
}
function animationGo7() {
    document.getElementById("monster").src = "./img/golemcha/golem7.png";
}
function animationGoStand() {
    document.getElementById("monster").src = "./img/golemcha/golem.png";
}
// ------------------------Golem ice---------------------------
function animationGoIce() {
    setTimeout(animationGoIce1, 100)
    setTimeout(animationGoIce2, 220)
    setTimeout(animationGoIce3, 260)
    setTimeout(animationGoIce4, 340)
    setTimeout(animationGoIce5, 420)
    setTimeout(animationGoIce6, 500)
    setTimeout(animationGoIce7, 580)
    setTimeout(animationGoIceStand, 660)
    

}
function animationGoIceDie() {
    setTimeout(animationGoIceDie1, 100)
    setTimeout(animationGoIceDie2, 180)
    setTimeout(animationGoIceDie3, 260)
    setTimeout(animationGoIceDie4, 340)
    setTimeout(animationGoIceDie5, 420)
    setTimeout(animationGoIceDie6, 500)
    setTimeout(animationGoIceDie7, 580)
    setTimeout(animationGoIceDie8, 660)
}
// ------------------------Golem ice atk-----------------------
function animationGoIce1() {
    document.getElementById("monster").src = "./img/icegolemcha/goice-1.png";
}
function animationGoIce2() {
    document.getElementById("monster").src = "./img/icegolemcha/goice-2.png";
}
function animationGoIce3() {
    document.getElementById("monster").src = "./img/icegolemcha/goice-3.png";
}
function animationGoIce4() {
    document.getElementById("monster").src = "./img/icegolemcha/goice-4.png";
}
function animationGoIce5() {
    document.getElementById("monster").src = "./img/icegolemcha/goice-5.png";
}
function animationGoIce6() {
    document.getElementById("monster").src = "./img/icegolemcha/goice-6.png";
}
function animationGoIce7() {
    document.getElementById("monster").src = "./img/icegolemcha/goice-7.png";
}
function animationGoIceStand() {
    document.getElementById("monster").src = "./img/icegolemcha/golem-2.png";
}
// -----------------------------Golem ice die------------------------
function animationGoIceDie1() {
    document.getElementById("monster").src = "./img/icegolemcha/die/golem-2-1.png";
}
function animationGoIceDie2() {
    document.getElementById("monster").src = "./img/icegolemcha/die/golem-2-2.png";
}
function animationGoIceDie3() {
    document.getElementById("monster").src = "./img/icegolemcha/die/golem-2-3.png";
}
function animationGoIceDie4() {
    document.getElementById("monster").src = "./img/icegolemcha/die/golem-2-4.png";
}
function animationGoIceDie5() {
    document.getElementById("monster").src = "./img/icegolemcha/die/golem-2-5.png";
}
function animationGoIceDie6() {
    document.getElementById("monster").src = "./img/icegolemcha/die/golem-2-6.png";
}
function animationGoIceDie7() {
    document.getElementById("monster").src = "./img/icegolemcha/die/golem-2-7.png";
}
function animationGoIceDie8() {
    document.getElementById("monster").src = "./img/icegolemcha/die/golem-2-8.png";
}

// ---------------------------Boss ---------------------------
function animationBoss() {
    setTimeout(animationBoss1, 100)
    setTimeout(animationBoss2, 220)
    setTimeout(animationBoss3, 260)
    setTimeout(animationBoss4, 340)
    setTimeout(animationBoss5, 420)
    setTimeout(animationBoss6, 500)
    setTimeout(animationBoss7, 580)
    setTimeout(animationBossStand, 660)
    

}
function animationBossDie() {
    setTimeout(animationBossDie1, 100)
    setTimeout(animationBossDie2, 180)
    setTimeout(animationBossDie3, 260)
    setTimeout(animationBossDie4, 340)
    setTimeout(animationBossDie5, 420)
    setTimeout(animationBossDie6, 500)
    setTimeout(animationBossDie7, 580)
    setTimeout(animationBossDie8, 660)
}
// ---------------------------Boss atk --------------------------
function animationBoss1() {
    document.getElementById("monster").src = "./img/bosscha/boss1.png";
}
function animationBoss2() {
    document.getElementById("monster").src = "./img/bosscha/boss2.png";
}
function animationBoss3() {
    document.getElementById("monster").src = "./img/bosscha/boss3.png";
}
function animationBoss4() {
    document.getElementById("monster").src = "./img/bosscha/boss4.png";
}
function animationBoss5() {
    document.getElementById("monster").src = "./img/bosscha/boss5.png";
}
function animationBoss6() {
    document.getElementById("monster").src = "./img/bosscha/boss6.png";
}
function animationBoss7() {
    document.getElementById("monster").src = "./img/bosscha/boss7.png";
}
function animationBossStand() {
    document.getElementById("monster").src = "./img/bosscha/boss.png";
}
// ---------------------------Boss die --------------------------
function animationBossDie1() {
    document.getElementById("monster").src = "./img/bosscha/die/bossdie1.png";
}
function animationBossDie2() {
    document.getElementById("monster").src = "./img/bosscha/die/bossdie2.png";
}
function animationBossDie3() {
    document.getElementById("monster").src = "./img/bosscha/die/bossdie3.png";
}
function animationBossDie4() {
    document.getElementById("monster").src = "./img/bosscha/die/bossdie4.png";
}
function animationBossDie5() {
    document.getElementById("monster").src = "./img/bosscha/die/bossdie5.png";
}
function animationBossDie6() {
    document.getElementById("monster").src = "./img/bosscha/die/bossdie6.png";
}
function animationBossDie7() {
    document.getElementById("monster").src = "./img/bosscha/die/bossdie7.png";
}
function animationBossDie8() {
    document.getElementById("monster").src = "./img/bosscha/die/bossdie8.png";
}
// -----------------------------------------------------------------------------
function hit1() {
    let health = document.getElementById("health")
    let btn = document.getElementById("btnnext")
    let btn2 = document.getElementById("btn")
    health.value -= 50;
    if (health.value <= 0) {
        animationGoDie();
        btn.style.visibility = "visible";
        btn2.style.visibility = "hidden";
    }else {
        rand();
    }
}

// -------------------------stage2-------------------------
function check2() {
    var int1 = document.getElementById('input1').value
    var n1 = parseInt(int1)
    var sumPlus = ran1 + ran2;
    var sumMinus = 0;
    if (ran1 < ran2) {
        sumMinus = ran2 - ran1;
    } else {
        sumMinus = ran1 - ran2;
    }
    if (isNaN(n1)) {
        document.getElementById('show').setAttribute('color', 'red')
        show.innerHTML = 'กรุณากรอกตัวเลขให้ครบ'
    } else if (plusMinus == '+' && n1 == sumPlus) {
        document.getElementById('show').setAttribute('color', 'yellow')
        show.innerHTML = 'ถูก'
        animationMc();
        hit2();
    } else if (plusMinus == '-' && n1 == sumMinus) {
        document.getElementById('show').setAttribute('color', 'yellow')
        show.innerHTML = 'ถูก'
        animationMc();
        hit2();
    } else {
        document.getElementById('show').setAttribute('color', 'red')
        show.innerHTML = 'ผิด'
        hpMc2();
        animationGo();
    }
}
function hpMc2() {
    let health1 = document.getElementById("health1")
    let btn = document.getElementById("btn")
    let btn2 = document.getElementById("btnplayagain")
    health1.value -= 25;
    if (health1.value <= 0) {
        mcDie();
        btn.style.visibility = "hidden";
        btn2.style.visibility = "visible";
    }
}
function hit2() {
    let health = document.getElementById("health")
    let btn = document.getElementById("btnnext")
    let btn2 = document.getElementById("btn")
    health.value -= 50;
    if (health.value <= 0) {
        animationGoDie();
        btn.style.visibility = "visible";
        btn2.style.visibility = "hidden";
    } else {
        rand();
    }
}
// -------------------------stage3-------------------------
function check3() {
    var int1 = document.getElementById('input1').value
    var n1 = parseInt(int1)
    var sumPlus = ran1 + ran2;
    var sumMinus = 0;
    if (ran1 < ran2) {
        sumMinus = ran2 - ran1;
    } else {
        sumMinus = ran1 - ran2;
    }
    if (isNaN(n1)) {
        document.getElementById('show').setAttribute('color', 'red')
        show.innerHTML = 'กรุณากรอกตัวเลขให้ครบ'
    } else if (plusMinus == '+' && n1 == sumPlus) {
        document.getElementById('show').setAttribute('color', 'yellow')
        show.innerHTML = 'ถูก'
        animationMc();
        hit3();
    } else if (plusMinus == '-' && n1 == sumMinus) {
        document.getElementById('show').setAttribute('color', 'yellow')
        show.innerHTML = 'ถูก'
        animationMc();
        hit3();
    } else {
        document.getElementById('show').setAttribute('color', 'red')
        show.innerHTML = 'ผิด'
        hpMc2();
        animationGoIce();
    }
}

function hit3() {
    let health = document.getElementById("health")
    let btn = document.getElementById("btnnext")
    let btn2 = document.getElementById("btn")
    health.value -= 25;
    if (health.value <= 0) {
        animationGoIceDie();
        btn.style.visibility = "visible";
        btn2.style.visibility = "hidden";
    }else {
        rand();
    }
}
// -------------------------stage4-------------------------
function check4() {
    var int1 = document.getElementById('input1').value
    var n1 = parseInt(int1)
    var sumPlus = ran1 + ran2;
    var sumMinus = 0;
    if (ran1 < ran2) {
        sumMinus = ran2 - ran1;
    } else {
        sumMinus = ran1 - ran2;
    }
    if (isNaN(n1)) {
        document.getElementById('show').setAttribute('color', 'red')
        show.innerHTML = 'กรุณากรอกตัวเลขให้ครบ'
    } else if (plusMinus == '+' && n1 == sumPlus) {
        document.getElementById('show').setAttribute('color', 'yellow')
        show.innerHTML = 'ถูก'
        animationMc();
        hit4();
    } else if (plusMinus == '-' && n1 == sumMinus) {
        document.getElementById('show').setAttribute('color', 'yellow')
        show.innerHTML = 'ถูก'
        animationMc();
        hit4();
    } else {
        document.getElementById('show').setAttribute('color', 'red')
        show.innerHTML = 'ผิด'
        hpMc2();
        animationGoIce();
    }
}
function hit4() {
    let health = document.getElementById("health")
    let btn = document.getElementById("btnnext")
    let btn2 = document.getElementById("btn")
    health.value -= 25;
    if (health.value <= 0) {
        animationGoIceDie();
        btn.style.visibility = "visible";
        btn2.style.visibility = "hidden";
    }else {
        rand();
    }
}
// -------------------------stageFinal-------------------------
function check5() {
    var int1 = document.getElementById('input1').value
    var n1 = parseInt(int1)
    var sumPlus = ran1 + ran2;
    var sumMinus = 0;
    if (ran1 < ran2) {
        sumMinus = ran2 - ran1;
    } else {
        sumMinus = ran1 - ran2;
    }
    if (isNaN(n1)) {
        document.getElementById('show').setAttribute('color', 'red')
        show.innerHTML = 'กรุณากรอกตัวเลขให้ครบ'
    } else if (plusMinus == '+' && n1 == sumPlus) {
        document.getElementById('show').setAttribute('color', 'yellow')
        show.innerHTML = 'ถูก'
        animationMc();
        hit5();
    } else if (plusMinus == '-' && n1 == sumMinus) {
        document.getElementById('show').setAttribute('color', 'yellow')
        show.innerHTML = 'ถูก'
        animationMc();
        hit5();
    } else {
        document.getElementById('show').setAttribute('color', 'red')
        show.innerHTML = 'ผิด'
        hpMc5();
        animationBoss();
    }
}
function hpMc5() {
    let health1 = document.getElementById("health1")
    let btn2 = document.getElementById("btnplayagain")
    health1.value -= 20;
    if (health1.value <= 0) {
        mcDie();
        btn.style.visibility = "hidden";
        btn2.style.visibility = "visible";
    }
}
function hit5() {
    let health = document.getElementById("health")
    let btn = document.getElementById("btnFinish")
    let btn2 = document.getElementById("btn")
    health.value -= 20;
    if (health.value <= 0) {
        animationBossDie();
        btn.style.visibility = "visible";
        btn2.style.visibility = "hidden";
    }else {
        rand();
    }
}